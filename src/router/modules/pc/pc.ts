
export default {
    path: '/pc',
    name: 'WebPage',
    component: () => {
        // @ts-ignore
        return import('@/components/pc/WebPage.vue')
    },
    meta: {
        title: 'WebPage',
    },
    children: [],
};
