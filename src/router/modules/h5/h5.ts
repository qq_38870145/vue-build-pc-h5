export default {
    path: '/h5',
    name: 'MobilePage',
    component: () => {
        // @ts-ignore
        return import('@/components/h5/MobilePage.vue')
    },
    meta: {
        title: 'MobilePage',
    },
    children: [],
};
