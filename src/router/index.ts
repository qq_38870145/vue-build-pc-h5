// @ts-ignore
import * as VueRouter from "vue-router";
// @ts-ignore
let modules = []
// @ts-ignore
const routeModuleList = []
// @ts-ignore
console.log('modules', modules)
// 环境变量
// @ts-ignore
const env = import.meta.env
console.log('env', env)
const { VITE_BUILD_MODE } = env
if (VITE_BUILD_MODE === 'h5') {
    // import.meta.glob() 直接引入所有的模块 Vite 独有的功能
    // @ts-ignore
    modules = import.meta.glob('./modules/h5/*.ts', { eager: true });
}
else if(VITE_BUILD_MODE === 'pc'){
    // import.meta.glob() 直接引入所有的模块 Vite 独有的功能
    // @ts-ignore
    modules = import.meta.glob('./modules/pc/*.ts', { eager: true });
}
else if(VITE_BUILD_MODE === 'dev'){
    // dev 开发模式pc和后所有路径都加载
    // @ts-ignore
    modules = import.meta.glob('./modules/**/*.ts', { eager: true });
}


// 加入到路由集合中
// @ts-ignore
Object.keys(modules).forEach((key) => {
// @ts-ignore
    const mod = modules[key].default || {};
    // @ts-ignore
    const modList = Array.isArray(mod) ? [...mod] : [mod];
    console.log('modList', modList)
    // @ts-ignore
    routeModuleList.push(...modList);
});


// @ts-ignore
console.log('routeModuleList', routeModuleList)
const router = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHashHistory(),
    // @ts-ignore
    routes: routeModuleList,
});
// 路由权限  beforeResolve
// @ts-ignore
router.beforeResolve(async (to, from, next) => {
    next()
});
export default router;
