// @ts-ignore
import { createApp } from 'vue'
// @ts-ignore
import App from './App.vue'
// 路由
// @ts-ignore
import router from "./router/index.ts";

// pc 端 element-plus
// @ts-ignore
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


// 移动端 vant
// 1. 引入你需要的组件
// @ts-ignore
import { Calendar,Cell } from 'vant';
// 2. 引入组件样式
import 'vant/lib/index.css';


const app=createApp(App)
app.use(router)

// 环境变量
// @ts-ignore
const env = import.meta.env
console.log('env', env)
const { VITE_BUILD_MODE } = env
if (VITE_BUILD_MODE === 'h5') {
// h5 组件
    app.use(Calendar)
    app.use(Cell)
}
else if(VITE_BUILD_MODE === 'pc'){
//pc 组件
    app.use(ElementPlus)
}
else if(VITE_BUILD_MODE === 'dev'){
// dev开发模式 h5 组件 pc组件都引入
    app.use(Calendar)
    app.use(Cell)
    app.use(ElementPlus)
}


app.mount('#app')
