// @ts-ignore
import { defineConfig,loadEnv } from 'vite'
import { resolve } from "path";
// @ts-ignore
import vue from '@vitejs/plugin-vue'
// https://vitejs.dev/config/
// @ts-ignore
export default defineConfig(({mode})=>{
  // 运行模式
  console.log('mode',mode)
  // 当前路径
  console.log('process.cwd()',process.cwd())
  // @ts-ignore
  const env=loadEnv(mode,process.cwd())
  console.log('env',env)


  return {
    // 打包相对路径
    base:['h5','pc'].includes(env.VITE_BUILD_MODE) ?'./':'/',
    build:{
      // 输出的目录  h5 pc
      outDir:'dist/'+env.VITE_BUILD_MODE
    },
    server: {
      host:true,
      // port: '3333',
      // open: true,
      // cors: true,
      // proxy: {
      //   ...proxy
      // },
    },

    resolve: {
      alias: {
        "@": resolve(__dirname, "src"),
      },
    },
    plugins: [
      vue()
    ],
  }
});
